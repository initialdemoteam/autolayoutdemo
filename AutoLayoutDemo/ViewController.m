//
//  ViewController.m
//  AutoLayoutDemo
//
//  Created by Nitin Maheshwari on 8/5/16.
//  Copyright © 2016 Cyber. All rights reserved.
//

#import "ViewController.h"



@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewOne;
@property (weak, nonatomic) IBOutlet UIView *viewTwo;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    NSDictionary *variableBindings = @{@"mainView" : self.view,
//                                       @"viewSomething" : self.viewOne};
    
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[viewOne]-0-|" options:NSLayoutFormatDirectionLeadingToTrailing metrics:nil views:variableBindings]];
    // width constraint
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[viewSomething(==250)]" options:0 metrics:nil views:variableBindings]];
//    
//    // height constraint
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[viewSomething(==20)]" options:0 metrics:nil views:variableBindings]];
//    
//    [self.viewOne addConstraint:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[view]-10-|" options:0 metrics:nil views:nil]];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
