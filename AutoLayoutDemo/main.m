//
//  main.m
//  AutoLayoutDemo
//
//  Created by Nitin Maheshwari on 8/5/16.
//  Copyright © 2016 Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
