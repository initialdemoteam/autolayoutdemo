//
//  AppDelegate.h
//  AutoLayoutDemo
//
//  Created by Nitin Maheshwari on 8/5/16.
//  Copyright © 2016 Cyber. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

